#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

struct Edge
{
	int source;
	int target;
	int dist;
	Edge( int source, int target, int dist ):source( source ), target( target ), dist( dist ) {};
};

struct Node
{
	int index;
	Node* parent;
	int height;
	Node()
	{
		parent = NULL;
		static int currentIndex = 1;
		index = currentIndex++;
	}
};

typedef pair<const int, Edge> pairEdge;
typedef multimap<const int, Edge>::iterator mapItr;

Node* find( Node* &x )
{
	{
		if ( !x->parent )
		{
			return x;
		}
		else
		{
			// Path Contraction
			if ( x->parent->parent )
			{
				x->parent = x->parent->parent;
			}
			return find( x->parent );
		}
	}
}

bool merge( Node* &x, Node* &y )
{
	Node* rootX = find( x );
	Node* rootY = find( y );
	if ( rootX == rootY )
	{
		return false;
	}
	else
	{
		// Link x, y
		if ( x->height >= y->height )
		{
			rootY->parent = rootX;
			int newBranchHeight = rootY->height + 1;
			rootX->height = ( rootX->height > newBranchHeight ? rootX->height : newBranchHeight );
		}
		else
		{
			rootX->parent = rootY;
			int newBranchHeight = rootX->height + 1;
			rootY->height = ( rootY->height > newBranchHeight ? rootY->height : newBranchHeight );
		}
		return true;
	}
}

int _tmain( int argc, _TCHAR* argv[] )
{
	int numClusters = 4;
	int dist;
	int source;
	int target;
	int numNodes;

	// init
	ifstream input( "clustering1.txt" );
	input >> numNodes;

	vector<Node*> nodes;
	for ( int count = 0; count < numNodes; count++ )
	{
		nodes.push_back( new Node() );
	}

	multimap<const int, Edge> edges;
	while ( input >> source >> target >> dist )
	{
		edges.insert( pairEdge( dist, Edge( source, target, dist ) ) );
	}

	int currentCluster = numNodes;

	//====================================//
	//  Pass one, merge until hits the
	//  required cluster number
	//====================================//

	mapItr itr;
	for ( itr = edges.begin(); itr != edges.end(); itr++ )
	{
		Edge* e = &( itr->second );
		if ( find( nodes[e->source - 1] ) != find( nodes[e->target - 1] ) )
		{
			if ( merge( nodes[e->source - 1], nodes[e->target - 1] ) )
			{
				currentCluster--;
			}
		}
		if ( currentCluster == numClusters )
		{
			break;
		}
	}

	//====================================//
	//  Pass two, start from where the
	//  itr left, and search for the
	//  smallest spacing
	//====================================//

	for ( ; itr != edges.end(); itr++ )
	{
		if ( find( nodes[itr->second.source - 1] ) != find( nodes[itr->second.target - 1] ) )
		{
			cout << "Maximized dist: " << itr->first << endl;
			break;
		}
	}

	return 0;
}